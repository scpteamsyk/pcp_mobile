sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("PCP_MOBILE.controller.App", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf PCP_MOBILE.view.App
		 */
		onInit: function() {

			var oModel = this.getOwnerComponent().getModel("services");
			var that = this;

			oModel.read("/issue", {
				urlParameters: {
					$filter: "NOTIFICATION_STATUS eq 'open'"
				},
				success: function(oData) {

					if (oData.results.length != 0) {
						that.getOwnerComponent().getRouter().navTo("detailWorkOrder");

						var oIssue = new sap.ui.model.json.JSONModel(oData.results[0]);
						that.getOwnerComponent().setModel(oIssue, "currentOpenIssue");
						var oNotificationStatus = new sap.ui.model.json.JSONModel({
							status: true
						});
						that.getOwnerComponent().setModel(oNotificationStatus, "notificationStatus");

					} else {
						that.getOwnerComponent().getRouter().navTo("notificationNotFound");
					}

				},
				error: function(oError) {
					jQuery.sap.log.debug("ERROR");
					jQuery.sap.log.debug(oError.toString());

				}

			});

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf PCP_MOBILE.view.App
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf PCP_MOBILE.view.App
		 */
		onAfterRendering: function() {
			$("#backBtn").remove();
		},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf PCP_MOBILE.view.App
		 */
		onExit: function() {
			$("#backBtn").remove();
		},
		
		isOnline: function(){
			var xhr = new (window.ActiveXObject || XMLHttpRequest) ("Microsoft.XMLHttpRequest");
			xhr.open("HEAD", "//" + window.location.hostname + "/?rand=" + Math.floor((Math.random() + 1) * 0x10000), false);
			
			try{
				xhr.send();
				return (xhr.status >= 200 && xhr.status < 300 || xhr.status === 304);
			}
			catch(error){
				return false; 
			}
		}

	});

});