sap.ui.define([
	"PCP_MOBILE/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("PCP_MOBILE.controller.Details", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf PCP_MOBILE.view.Details
		 */
		onInit: function() {
			this.getRouter();
		},
		
		onAfterRendering: function(){
			$("span[id$='location2']").prepend("<b>Lindahl<b>");
		},
		
		onPressToolbox: function(){
			this.getOwnerComponent().getRouter().navTo("toolbox");
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf PCP_MOBILE.view.Details
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf PCP_MOBILE.view.Details
		 */
		//	onAfterRendering: function() {
		//
		//	},
 
		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf PCP_MOBILE.view.Details
		 */
		//	onExit: function() {
		//
		//	}
 
	});

});