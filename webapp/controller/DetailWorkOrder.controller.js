sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("PCP_MOBILE.controller.DetailWorkOrder", {
		onInit: function() {

/*			var oModel = this.getOwnerComponent().getModel("services");
			var that= this;

			oModel.read("/issue", {
				urlParameters: {
					$filter: "TICKET_STATUS eq 'open'"
				},
				success: function(oData) {
					
					var status = false;
					
					if(oData.results.length != 0)
						status = true;
						
					var areThereOpenTicket = new sap.ui.model.json.JSONModel({
						value: status
					}); 
					
					that.getOwnerComponent().setModel(areThereOpenTicket, "areThereOpenTicket");

				},
				error: function(oError) {
					jQuery.sap.log.debug("ERROR");
					jQuery.sap.log.debug(oError.toString());

				}

			});*/

		},
		onAfterRendering: function() {
			$("span[id$='location']").prepend("<b>Lindahl<b>");
			$("#backBtn").remove();
		},
		handlePopoverPress: function(oEvent) {

			// create popover
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("PCP_MOBILE.fragment.Popover", this);
				this.getView().addDependent(this._oPopover);
			}

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function() {
				this._oPopover.openBy(oButton);
			});
		},

		onAccept: function() {
			this.getOwnerComponent().getRouter().navTo("details");
		},

		onActivityPress: function() {
			this.getOwnerComponent().getRouter().navTo("activityList");
		}
	});
});