sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/GroupHeaderListItem",
	"sap/ui/model/json/JSONModel"
], function(Controller, GroupHeaderListItem, JSONModel) {
	"use strict";

	return Controller.extend("PCP_MOBILE.controller.Toolbox", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf PCP_MOBILE.view.Toolbox
		 */
		onInit: function(evt) {
			// set explored app's demo model on this sample
			var oModel = new JSONModel(jQuery.sap.getModulePath("PCP_MOBILE.mockdata", "/materials_and_tools.json"));
			this.getView().setModel(oModel);
		},

		getGroupHeader: function(oGroup) {
			return new GroupHeaderListItem({
				title: oGroup.key,
				upperCase: false
			});
		},

		onClickReject: function() {
			this.getOwnerComponent().getRouter().navTo("detailWorkOrder");
/*			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			oCrossAppNavigator.toExternal({
				target: {
					semanticObject: "#"
				}
			});*/
		},

		onClickAccept: function() {
			var oNotificationStatus = new sap.ui.model.json.JSONModel({
				status: false
			});

			var oModel = this.getOwnerComponent().getModel("services");

			var currentIssue = this.getOwnerComponent().getModel("currentOpenIssue").oData;
			var sWorkOrderId = (Math.floor(Math.random() * (40)) + 40).toString();
			var sNotificationId = (Math.floor(Math.random() * (40)) + 10).toString();
			
			var oIssue = {
				"ISSUE_ID": currentIssue.ISSUE_ID,
				"TICKET_STATUS": "closed",
				"ORDER": sWorkOrderId,
				"ORDER_STATUS": "to approve",
				"STATUS": "wo open",
				"NOTIFICATION_STATUS": "closed"
			};
			var sPath = "/issue(ISSUE_ID='" + currentIssue.ISSUE_ID + "')";
			oModel.update(sPath, oIssue, null, function(oData) {}, function(oError) {	jQuery.sap.log.debug(oError.toString());});
			

			
			var oWorkOder = {
				"ORDER": sWorkOrderId,
				"PLANTID": currentIssue.PLANTID,
				"FUNCTIONAL_LOCATION_GU": currentIssue.FUNCTIONAL_LOCATION_GU,
				"MAINTITEM": "10000000002536",
				"NOTIFICATION_ID": "44455",
				"ORDER_TYPE": "M1",
				"ORDER_STATUS": "to approve",
				"CREATED_ON": "03/07/2017",
				"ACTUAL_RELEASE": "",
				"BASIC_FIN_DATE": "05/07/2017",
				"ACTUAL_START": "",
				"ACTUAL_FINISH": "",
				"SCHED_FINISH": "05/07/2017",
				"SCHED_START": "05/07/2017",
				"COST_CENTER": "ABWDH1000P",
				"WBS_ELEMENT": "",
				"WBS_ORD_HEADER": "ABPPWDH100YAB14M10000",
				"DESCRIPTION": "AB14-Transformers Monthly Visual Inspec.",
				"SERIAL_NUMBER": "",
				"TOTAL_PINND_COSTS": "0",
				"TOTAL_ACT_COSTS": "0",
				"SYSTEM_STATUS": "CRTD NMAT NTUP PRC",
				"BAS_START_DATE": "04/07/2017",
				"ENETERED_BY": "IP1020170703",
				"TOT_SUM_PLAN": "0",
				"WORK_CENTER": "I_EUVOIA",
				"RES_PURC_REQ": "2",
				"COMPANY_CODE": "GRAB",
				"MAINT_ACTIV_TYPE": "CI",
				"PLANT": "ABP1",
				"SYSTEM_CONDITION": "0",
				"PO_NUMBER": "",
				"MATERIAL": "",
				"LOCATION": "00",
				"LOCATION_DESCRIPTION": "MV"
			};

			var that = this;
			oModel.create("/workorder", oWorkOder, null, function(oData) {
				that.getOwnerComponent().setModel(oNotificationStatus, "notificationStatus");
			}, function(oError) {	jQuery.sap.log.debug(oError.toString());});

			this.getOwnerComponent().setModel(oNotificationStatus, "notificationStatus");
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			oCrossAppNavigator.toExternal({
				target: {
					semanticObject: "#"
				}
			});
			/*			this.getOwnerComponent().getRouter().navTo("notificationNotFound");*/
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf PCP_MOBILE.view.Toolbox
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf PCP_MOBILE.view.Toolbox
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf PCP_MOBILE.view.Toolbox
		 */
		//	onExit: function() {
		//
		//	}

	});

});