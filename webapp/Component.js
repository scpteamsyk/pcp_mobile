sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"PCP_MOBILE/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("PCP_MOBILE.Component", {

		metadata: {
			manifest: "json" 
		},
		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			this.getRouter().initialize();
			
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			var oRootPath = jQuery.sap.getModulePath("PCP_MOBILE"); // your resource root
		
			var oRootPathModel = new sap.ui.model.json.JSONModel({
				path : oRootPath
			});
		
			this.setModel(oRootPathModel, "rootPath");
			
/*			setInterval(function(){
				$("#backBtn").remove();
			}, 1000);*/
		}
	});
});